/**
 * @file StateMachine.hpp
 * @author Julian Neundorf
 * @brief
 * @version 0.1
 * @date 2024-01-04
 *
 * Remarks:
*/
#pragma once

#include <cstdint>

extern std::uint32_t getMillis();

template<typename T>
class StateMachine {
public:
  StateMachine() = default;

  T getStatus() const {
    return status_;
  };

  std::uint32_t getLastChange() const {
    return lastModification_;
  };

  std::uint32_t getDiffLastChange() const {
    return getMillis() - lastModification_;
  };

protected:
  void setStatus(T _status) {
    lastModification_ = getMillis();
    status_ = _status;
  }

private:
  T status_{};
  std::uint32_t lastModification_ = 0;
};